package ru.tsc.denisturovsky.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.denisturovsky.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;

import java.util.Date;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> implements ITaskDTORepository {

    @NotNull
    @Override
    public TaskDTO create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final TaskDTO task = new TaskDTO(name, description);
        return add(userId, task);
    }

    @NotNull
    @Override
    public TaskDTO create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws Exception {
        @NotNull final TaskDTO task = new TaskDTO(name);
        return add(userId, task);
    }

    @NotNull
    @Override
    public TaskDTO create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Date dateBegin,
            @NotNull final Date dateEnd
    ) throws Exception {
        @NotNull final TaskDTO task = new TaskDTO(name, description, dateBegin, dateEnd);
        return add(userId, task);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) throws Exception {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, getEntityClass())
                            .setParameter("userId", userId)
                            .setParameter("projectId", projectId)
                            .getResultList();
    }

}
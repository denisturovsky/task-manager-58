package ru.tsc.denisturovsky.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.denisturovsky.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.denisturovsky.tm.dto.model.SessionDTO;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> implements ISessionDTORepository {

}
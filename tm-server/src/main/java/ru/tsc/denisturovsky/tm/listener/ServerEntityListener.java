package ru.tsc.denisturovsky.tm.listener;

import lombok.NoArgsConstructor;
import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.log.OperationEvent;
import ru.tsc.denisturovsky.tm.log.OperationType;

@NoArgsConstructor
public final class ServerEntityListener implements PostInsertEventListener, PostDeleteEventListener, PostUpdateEventListener {

    @Nullable
    private JmsLoggerProducer jmsLoggerProducer;

    public ServerEntityListener(@NotNull final JmsLoggerProducer jmsLoggerProducer) {
        this.jmsLoggerProducer = jmsLoggerProducer;
    }

    private void log(
            @NotNull final OperationType type,
            @NotNull final Object entity
    ) {
        jmsLoggerProducer.send(new OperationEvent(type, entity));
    }

    @Override
    public void onPostDelete(@NotNull final PostDeleteEvent event) {
        log(OperationType.DELETE, event.getEntity());
    }

    @Override
    public void onPostInsert(@NotNull final PostInsertEvent event) {
        log(OperationType.INSERT, event.getEntity());
    }

    @Override
    public void onPostUpdate(@NotNull final PostUpdateEvent event) {
        log(OperationType.UPDATE, event.getEntity());
    }

    @Override
    public boolean requiresPostCommitHanding(@NotNull final EntityPersister persister) {
        return false;
    }

}

package ru.tsc.denisturovsky.tm.sevice.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.denisturovsky.tm.api.repository.model.ISessionRepository;
import ru.tsc.denisturovsky.tm.api.service.model.ISessionService;
import ru.tsc.denisturovsky.tm.model.Session;

@Service
@NoArgsConstructor
public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    protected ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }

}
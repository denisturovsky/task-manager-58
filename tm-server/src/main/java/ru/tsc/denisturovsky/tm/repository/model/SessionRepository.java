package ru.tsc.denisturovsky.tm.repository.model;

import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.denisturovsky.tm.api.repository.model.ISessionRepository;
import ru.tsc.denisturovsky.tm.model.Session;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

}
